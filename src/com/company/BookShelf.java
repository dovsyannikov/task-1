package com.company;

import java.util.Scanner;

public class BookShelf {
    Books[] book = new Generator().generateBook();

    public void findAndPrintOldestBook(){
        Books oldestBook = book[0];

        //найти самую старую книгу и вывести ее автора
        for (int i = 0; i < book.length; i++) {
            if (oldestBook.isOlderThen(book[i])) {
                oldestBook = book[i];
            }
        }
        System.out.println("The name of the author of the oldest book in list is: " + oldestBook.author.toUpperCase());
    }
    public void findAndPrintBookNewerThan(int year){
        for (Books book : book) {
            if (book.isPublishAfter(year)) {
                book.print();
            }
        }
    }
    public void findAndPrintBookWithSpecificAuthor(String specificAuthor){
        for (int i = 0; i < book.length; i++) {
            if (book[i].author.toLowerCase().equals(specificAuthor.toLowerCase())) {
                System.out.println(String.format("книги автора %s :", specificAuthor.toUpperCase()) + book[i].nameOfTheBook.toUpperCase());
            }
        }
    }
}
