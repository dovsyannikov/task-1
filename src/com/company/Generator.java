package com.company;

public class Generator {
    public Books[] generateBook() {
        Books[] book = new Books[5];

        book[0] = new Books(1719, "Robinson Crusoe", "daniel defoe");
        book[1] = new Books(1869, "War and Peace", "Leo Tolstoy");
        book[2] = new Books(1862, "The House of the Dead", "Fyodor Dostoevsky");
        book[3] = new Books(1925, "The Great Gatsby", "F Scott Fitzgerald");
        book[4] = new Books(1856, "The Snowstorm", "Leo Tolstoy");
        return book;
    }
}
