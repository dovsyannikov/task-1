package com.company;

import java.awt.print.Book;

public class Books {
    public String nameOfTheBook = new String();
    public String author = new String();
    public int createdDate;
    public Books(){

    }
    public Books(int createdDate, String nameOfTheBook, String author){
        this.createdDate = createdDate;
        this.nameOfTheBook = nameOfTheBook;
        this.author = author;
    }

    public boolean isOlderThen(Books book){
        return createdDate > book.createdDate;
    }
    public boolean isPublishAfter(int year){
        return createdDate < year;
    }
    public void print(){
        System.out.println(author + " : " + nameOfTheBook + " // " + createdDate);
    }
    public String compareAuthorWithKeyWord(String authorName){
        return authorName.toLowerCase();
    }

}
