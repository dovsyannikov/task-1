package com.company;

import java.util.Locale;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Task1();
    }

    public static void Task1() {
        //1. Дан список книг. Книга содержит название, автора и год издания.
        // Необходимо: найти самую старую книгу и вывести ее автора; найти книгу определенного автора и вывести ее название
        // (или названия); найти книги, изданные ранее введенного года и вывести всю информацию по ним.

        System.out.println("Task 1");

        // Необходимо: найти самую старую книгу и вывести ее автора;
        BookShelf bookshelf = new BookShelf();
        bookshelf.findAndPrintOldestBook();


//      //найти книгу определенного автора и вывести ее название
//      //(или названия);
        bookshelf.findAndPrintBookWithSpecificAuthor("Leo Tolstoy");

        // найти книги, изданные ранее введенного года и вывести всю информацию по ним.
        Scanner scanner = new Scanner(System.in);
        System.out.println(String.format("Ранее какого года вы хотите увидеть (%d минимальный год) перечень книг?", 1720));
        int minYear = Integer.parseInt(scanner.nextLine());
        bookshelf.findAndPrintBookNewerThan(minYear);
    }

    public static void Task1dot1(){
//        1.1 Дополнение к первому заданию - книга дополнительно содержит издательство,
//        может содержать нескольких авторов. Необходимо: найти книги определенного автора,
//        написанные в соавторстве с кем-то (т.е. он не единственный автор книги) и вывести всю инфу по ним;
//        найти книги, где 3 и более авторов и вывести всю инфу по ним.
    }
}
